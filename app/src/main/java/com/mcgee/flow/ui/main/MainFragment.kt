package com.mcgee.flow.ui.main

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.widget.doAfterTextChanged
import androidx.lifecycle.ViewModelProvider
import com.mcgee.flow.R
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.lifecycleScope
import kotlinx.android.synthetic.main.main_fragment.*
import kotlinx.coroutines.channels.consumeEach
import kotlinx.coroutines.launch

class MainFragment : Fragment() {
    //private var binding:ViewDataBinding? = null
    companion object {
        fun newInstance() = MainFragment()
    }

    private lateinit var viewModel: MainViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.main_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        //binding = DataBindingUtil.findBinding<ViewDataBinding>(textInputLayout)
        viewModel = ViewModelProvider(this).get(MainViewModel::class.java)
        //.searchChannel.observe(this) { handleSearchResult(it) }
        viewModel.searchChannel.openSubscription()

        textInputEditText.doAfterTextChanged { editable ->
            lifecycleScope.launch {
                viewModel.searchChannel.send(editable.toString())
            }
        }
        lifecycleScope.launch{
            viewModel.searchChannel.consumeEach {
                Log.d("LearningChannel",it)
            }
        }
    }

    override fun onDetach() {
        super.onDetach()
        viewModel.searchChannel.cancel()
    }

}
