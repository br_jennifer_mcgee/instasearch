package com.mcgee.flow.ui.main

import androidx.lifecycle.ViewModel
import kotlinx.coroutines.channels.BroadcastChannel
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.asFlow

class MainViewModel : ViewModel() {
    val searchChannel = BroadcastChannel<String>(Channel.CONFLATED)
}
